package fisherman

import (
	"fmt"
	"strings"
)

type TaskInfo struct {
	ServiceName string
	TaskSlot    string
	TaskID      string
	IP          string
	NetworkName string
	HostName    string
}

func (t *TaskInfo) String() string {
	res := []string{}
	res = append(res, fmt.Sprintf("%-15s : %s", "ServiceName", t.ServiceName))
	res = append(res, fmt.Sprintf("%-15s : %s", "TaskSlot", t.TaskSlot))
	res = append(res, fmt.Sprintf("%-15s : %s", "TaskID", t.TaskID))
	return strings.Join(res, "\n")
}
