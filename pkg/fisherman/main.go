package fisherman

import (
	"bytes"
	"fmt"
	"log"
	"net"
	"os"
	"sort"
	"strings"
	"text/template"
	"time"

	"github.com/urfave/cli"
)

func levelToInt(level string) int {
	switch level {
	case "error":
		return 3
	case "warn":
		return 4
	case "notice":
		return 5
	case "info":
		return 6
	case "debug":
		return 7
	default:
		fmt.Printf("Can not resolve '%s' to level integer\n", level)
		os.Exit(1)
	}
	return 0
}

var (
	fishermanTemplates = map[string]string{
		"etcd":     "{{.ServiceName}}.{{.TaskSlot}}.{{.TaskID}}=http://{{.IP}}:2380",
		"hostlist": "{{.ServiceName}}.{{.TaskSlot}}.{{.TaskID}}",
	}
)

type Fisherman struct {
	Ctx *cli.Context
}

func NewFisherman(ctx *cli.Context) Fisherman {
	return Fisherman{
		Ctx: ctx,
	}
}

func (f *Fisherman) Log(level, msg string) {
	lint := levelToInt(level)
	logLevel := levelToInt(f.Ctx.String("log-level"))
	if logLevel >= lint {
		log.Printf("[%-5s] %s", level, msg)
	}
}

func (f *Fisherman) resolveTask(ip string) (ti TaskInfo, err error) {
	addrs, _ := net.LookupAddr(ip)
	if len(addrs) == 0 {
		return ti, fmt.Errorf("could not identify exctly on task for '%s': got %v", ip, addrs)
	}
	slice := strings.Split(addrs[0], ".")
	if len(slice) < 3 {
		err = fmt.Errorf("hostname should have 3 parts, devided by '.': %v", addrs)
		f.Log("error", err.Error())
	}
	ti.IP = ip
	ti.ServiceName = slice[0]
	ti.TaskSlot = slice[1]
	ti.TaskID = slice[2]
	ti.HostName = fmt.Sprintf("%s.%s.%s", ti.ServiceName, ti.TaskSlot, ti.TaskID)
	f.Log("debug", fmt.Sprintf("Resolved '%s' to '%s'", ip, ti))
	return
}

func (f *Fisherman) createHealthCheckOverwrite() {
	hdir := f.Ctx.String("healthcheck-dir")
	fpath := fmt.Sprintf("%s/force_true", hdir)
	if _, err := os.Stat(fpath); err == nil {
		return
	}
	w, err := os.Create(fpath)
	if err != nil {
		panic(err)
	}
	defer w.Close()

	f.Log("debug", fmt.Sprintf("File '%s' created!", fpath))
}

func (f *Fisherman) fetchIPs() []string {
	mintasks := f.Ctx.Int("mintasks")
	delay := time.Duration(f.Ctx.Int("delay")) * time.Second
	srv := f.Ctx.Args().Get(0)
	ips := []string{}
	_ = ips
	for {
		q := fmt.Sprintf("tasks.%s", srv)
		ips, _ = net.LookupHost(q)
		sort.Strings(ips)
		if mintasks == 0 {
			if len(ips) == 0 {
				fmt.Printf("Could not find IPs for '%s'\n", q)
				os.Exit(1)
			}
			break

		} else if len(ips) < mintasks {
			f.Log("debug", fmt.Sprintf("Only found %d ips so far %v, expect %d", len(ips), ips, mintasks))
			f.createHealthCheckOverwrite()
			time.Sleep(delay)
			continue
		} else {
			break
		}
	}
	f.Log("debug", fmt.Sprintf("IPs found for service '%s': %v", srv, ips))
	return ips
}

func (f *Fisherman) Run() {
	srv := f.Ctx.Args().Get(0)
	res := []string{}
	printTaskIP := f.Ctx.Bool("print-task-ip")
	printContainerIP := f.Ctx.Bool("print-container-ip")
	hostname, _ := os.Hostname()
	if printContainerIP {
		ips, _ := net.LookupHost(hostname)
		fmt.Printf("%s\n", ips[0])
		os.Exit(0)
	}
	if srv == "" {
		f.Log("error", "please provide service name to look for as an argument.")
		os.Exit(1)
	}
	out := f.Ctx.String("out")
	tmpl := f.Ctx.String("template")
	if val, ok := fishermanTemplates[tmpl]; ok {
		f.Log("debug", fmt.Sprintf("evaluated '%s' to '%s'", tmpl, val))
		tmpl = val

	}
	ips := f.fetchIPs()
	for _, ip := range ips {
		task, err := f.resolveTask(ip)
		if printTaskIP && hostname == task.HostName {
			fmt.Println(task.IP)
			os.Exit(0)
		}
		if err != nil {
			f.Log("error", err.Error())
			test, _ := net.LookupAddr(ip)
			f.Log("debug", strings.Join(test, " "))
		}
		f.Log("debug", fmt.Sprintf("Running Template: %s", tmpl))
		t := template.Must(template.New("tmpl").Parse(tmpl))
		buf := new(bytes.Buffer)
		err = t.Execute(buf, task)
		if err != nil {
			f.Log("error", fmt.Sprintf("Error during rendering '%s': %s", tmpl, err.Error()))
		}
		res = append(res, buf.String())
	}
	switch out {
	case "bash":
		fmt.Println(strings.Join(res, " "))
	case "list":
		fmt.Println(strings.Join(res, ","))
	default:
		f.Log("error", fmt.Sprintf("'%s' is not a valid output format. (bash)", out))
	}

}
