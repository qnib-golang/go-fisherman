package main

import (
	"os"

	"github.com/qnib/go-fisherman/pkg/fisherman"
	"github.com/urfave/cli"
	"github.com/zpatrick/go-config"
)

func Run(ctx *cli.Context) {
	cfg := config.NewConfig([]config.Provider{})
	cfg.Providers = append(cfg.Providers, config.NewCLI(ctx, false))
	f := fisherman.NewFisherman(ctx)
	f.Run()
}

func main() {
	app := cli.NewApp()
	app.Name = "Golang library to help fishing information from moby"
	app.Usage = "go-fishermen [options] <service>"
	app.Version = "0.0.5"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "out,o",
			Value:  "bash",
			Usage:  "Output format (only 'bash' or 'list' for now).",
			EnvVar: "FISHERMAN_OUT",
		},
		cli.StringFlag{
			Name:   "template,t",
			Value:  "{{.IP}}",
			Usage:  "Golang Template. (e.g. '{{.ServiceName}}.{{.TaskSlot}}.{{.TaskID}}={{.IP}}')",
			EnvVar: "FISHERMAN_TEMPLATE",
		},
		cli.IntFlag{
			Name:   "mintasks",
			Value:  0,
			Usage:  "Expected amount of task to discover. While in this mode the local healhcheck will be set to TRUE (0: disable)",
			EnvVar: "FISHERMAN_MIN_TASKS",
		},
		cli.IntFlag{
			Name:   "delay",
			Value:  1,
			Usage:  "Delay (seconds) between lookups",
			EnvVar: "FISHERMAN_DELAY",
		},
		cli.StringFlag{
			Name:   "healthcheck-dir",
			Value:  "/opt/healthcheck/",
			Usage:  "Healhcheck directory in which ./force_true overwrites the healthcheck to become true",
			EnvVar: "HEALTHCHECK_DIR",
		},
		cli.BoolFlag{
			Name:   "healthcheck-overwrite",
			Usage:  "If set the healthcheck can be overwriten if mintasks is not 0",
			EnvVar: "ALLOW_HEALTHCHECK_OVERWRITE",
		},
		cli.BoolFlag{
			Name:  "print-task-ip",
			Usage: "Prints the IP of the task matching the hostname",
		},
		cli.BoolFlag{
			Name:  "print-container-ip",
			Usage: "Prints the IP of the container",
		},
		cli.StringFlag{
			Name:   "log-level",
			Value:  "warn",
			Usage:  "Log level (warn: silent)",
			EnvVar: "LOG_LEVEL",
		},
	}
	app.Action = Run
	app.Run(os.Args)
}
